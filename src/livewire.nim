import
  os,
  strutils,
  terminal,
  cwd

var
  leftTerminator = ""
  leftSeparator = ""
  leftPrompt = getCurrentDir()

proc formatCwd: string =
  return getCurrentDir()

proc printPrompt: void =
  stdout.styledWriteLine bgRed, fgCyan, " ", collapsePath(getCurrentDir(), 30), " ", leftTerminator

when isMainModule:
  printPrompt()
