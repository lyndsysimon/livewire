import
  os,
  sequtils,
  strutils,
  unicode

proc collapsePath*(path: string, maxLength: int): string =
  if path.len <= maxLength: return path

  var parts = path.split("/")

  if parts[^1].len > maxLength-1:
    return "…" & parts[^1][^maxLength+1..^1]

  for i, x in parts:
    if i == 0 or i == parts.len-1: continue
    parts[i] = $x[0]
    var newPath = parts.join("/")
    if newPath.len <= maxLength: return newPath

  return "UNHANDLED"

when isMainModule:
  import unittest

  let
    absolutePath = "/foo/bar/baz/qiz/buzz"
    relativePath = "~/foo/bar/baz/qiz/buzz"

  suite "collapsePath":
    test "short paths are unchanged":
      check:
        collapsePath(absolutePath, 100) == absolutePath
        collapsePath(relativePath, 100) == relativePath

    test "long last pieces are truncated":
      check:
        collapsePath("/foo/this-is-a-long-path-segment", 8) == "…segment"

    test "long paths are shortened":
      check:
        collapsePath(absolutePath, 13) == "/f/b/b/q/buzz"
        collapsePath(relativePath, 14) == "~/f/b/b/q/buzz"
        collapsePath(absolutePath, 17) == "/f/b/baz/qiz/buzz"
        collapsePath(relativePath, 18) == "~/f/b/baz/qiz/buzz"
